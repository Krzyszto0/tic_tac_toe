import java.util.Scanner;

public class Kolko_i_krzyzyk {
    public static char board[][] = new char[3][3];

    public static int Input(){
    Scanner input = new Scanner(System.in);
    int in = input.nextInt();
    return in;
    }

    public static void initializeBoard(){
       for (int i = 0; i < board.length; i++){
        for (int j = 0; j < board.length; j++){
            board[i][j] = ' ';
        }
       }
    }

    public static void displayNumbers(){
    int k = 0;
    for(int i=0; i < board.length; i++){
        System.out.print("|");
        for(int j=0; j < board.length; j++){
            System.out.print((i*3+j+1) + "|");
        }
        System.out.println();
        while (k < 3) {
            System.out.print(" -");
            k++;
        }
        System.out.println();
        k = 0;
    }
}

public static void displayBoard(){
    int k = 0;
    for(int i=0; i < board.length; i++){
        System.out.print("|");
        for(int j=0; j < board.length; j++){
            System.out.print(board[i][j] + "|");
        }
        System.out.println();
        while (k < 3) {
            System.out.print(" -");
            k++;
        }
        System.out.println();
        k = 0;
    }
}

public static boolean isMoveValid(int i, int j){
    if(board[i][j] != ' '){
        System.out.println("Pole jest zajęte, nie można wykonać ruchu");
        System.out.println("Podaj inne pole");
        return false;
    }
    return true;
}

public static boolean ValidMove(int i, int j, char player){
    if(isMoveValid(i, j) == true){
        board[i][j]= player;
        displayBoard();
        return true;
        }
    return false;
}

public static boolean WhoWon(char player){
    return checkColumns(player) || checkrows(player) || checkDiagonal(player);
}

public static boolean checkColumns(char player){
for(int i = 0; i < board.length; i++){
    if (board[0][i] == player && board[1][i] == player && board[2][i] == player){
        Win(player);
        return true;
    }
}
    return false;
}

public static boolean checkrows(char player){
for(int i = 0; i < board.length; i++){
    if (board[i][0] == player && board[i][1] == player && board[i][2] == player){
        Win(player);
        return true;
    }
}
    return false;
}

public static boolean checkDiagonal(char player){
    if (board[0][0] == player && board[1][1] == player && board[2][2] == player){
        Win(player);
        return true;
    }else if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
        Win(player);
        return true;   
    }
    return false;
}

public static void Win(char player){
    System.out.println("Wygrywa "+player+"!!!!");
}

public static void ComputerMove(char player){
    int row, col;
    if(player == 'X'){
        do{
            row = (int)(Math.random()*3);
            col = (int)(Math.random()*3);
        }while(!ComputerValidMove(row, col));
        board[row][col] = 'O';
    }else if(player == 'O'){
        do{
            row = (int)(Math.random()*3);
            col = (int)(Math.random()*3);
        }while(!ComputerValidMove(row, col));
        board[row][col] = 'X';
    }
    displayBoard();
}

public static boolean ComputerValidMove(int row, int col){
    if(board[row][col] != ' '){
        return false;
    }
    return true;
}

public static void Move(int i, int j, char player){
    boolean valid = ValidMove(i, j, player);
    WhoWon(player);
    if (WhoWon(player) == false && valid == true) {
        ComputerMove(player);        
    }
}

public static void makeMove(char player){
    int i, j;
    int move = Input();
    switch (move) {
        case 1:
            i = 0;
            j = 0;
            Move(i, j, player);
            break;
        case 2:
            i = 0;
            j = 1;
            Move(i, j, player);
            break;
        case 3:
            i = 0;
            j = 2;
            Move(i, j, player);
            break;
        case 4:
            i = 1;
            j = 0;
            Move(i, j, player);
            break;
        case 5:
            i = 1;
            j = 1;
            Move(i, j, player);
            break;
        case 6:
            i = 1;
            j = 2;
            Move(i, j, player);
            break;
        case 7:
            i = 2;
            j = 0;
            Move(i, j, player);
            break;
        case 8:
            i = 2;
            j = 1;
            Move(i, j, player);
            break;
        case 9:
            i = 2;
            j = 2;
            Move(i, j, player);
            break;
    }
}
    public static void main(String[] args) {
    initializeBoard();
    int choose = 0;
    boolean gameWon = false;
    
       char player;
       displayNumbers();
       System.out.print("               Wybierz swojego piona: \n");
       System.out.print("0 - gracz rozpoczynający(X)  |   1 - gracz drugi (O) \n");
       choose = Input();

       switch (choose) {
        case 0:
        player = 'X';
        System.out.println("Wybrałeś X, wprowadź numer gdzie chcesz go postawić");
        
        while (!gameWon) {          
            System.out.println("WWybierz numer pola:");
            displayNumbers();
            makeMove(player);
            gameWon = WhoWon(player);
        }
            break;
        case 1:
        player = 'O';
        System.out.println("Wybrałeś O, wprowadź numer gdzie chcesz go postawić");
        while (!gameWon) {
            System.out.println("Wybierz numer pola:");
            displayNumbers();
            makeMove(player); 
            gameWon = WhoWon(player);
        }
            break;
       }
    System.out.println("Who won?????");
}
}